
var today = new Date();
var currentMonth = today.getMonth();
var currentYear = today.getFullYear();
var selectYear = document.getElementById('year');
var selectMonth = document.getElementById('month');

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var monthAndYear = document.getElementById('month-and-year');

// create year options for user to select
var iYear = 1994;
for (var i = 0; i < 30; i++) {
	var option = document.createElement('option');
	option.value = iYear;
	option.innerHTML = iYear;
	iYear++
	selectYear.appendChild(option);
}

showCalendar(currentMonth, currentYear);


function next() {
	currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
	currentMonth = (currentMonth === 11) ? 1 : currentMonth + 1;
	showCalendar(currentMonth, currentYear);
}

function prev() {
	currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
	currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
	showCalendar(currentMonth, currentYear);
}


function jump() {
	currentYear = parseInt(selectYear.value)
	currentMonth = parseInt(selectMonth.value)
	showCalendar(currentMonth, currentYear);
}

function showCalendar(month, year) {
	var firstDay = (new Date(year, month)).getDay();
	var table = document.getElementById('calendar-body');

	// clear previous cells
	table.innerHTML = '';
	selectYear.value = year;
	selectMonth.value = month;
	monthAndYear.innerHTML = months[month] + ' ' + year;

	var dayOfMonth = 1;
	for (var i = 0; i < 6; i++) { // maximum number of weeks in a month
		// create a table row (one week)
		var row = document.createElement('tr');

		// populate the days of the week
		for (var j = 0; j < 7; j++) {
			if (dayOfMonth > daysInMonth(month, year)) {
				break;
			}

			var cell = document.createElement('td');
			cell.className = "selectable-date";
			cell.onclick = selectDate;

			if (dayOfMonth === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
				cell.classList.add('today')
			}

			// if 1st of month is not a sunday, create empty cells at start of 1st week
			if (i === 0 && j < firstDay) {
				cell.appendChild(document.createTextNode(''));
			} else {
				cell.appendChild(document.createTextNode(dayOfMonth));
				dayOfMonth++
			}
			row.appendChild(cell);
		}
		table.appendChild(row);
	}
}

function selectDate(e) {
	var form = document.getElementById('datefield');
	form.value = formatDate(e.target.innerHTML, currentMonth, currentYear);
	previousSelect = document.getElementsByClassName('selected');
	if (previousSelect.length > 0) previousSelect[0].classList.remove('selected');
	e.target.classList.add('selected');
}

function formatDate(day, month, year) {
	if (day < 10) day = '0' + day;
	if (month < 10) month = '0' + month;
	return `${day}-${month}-${year}`;
}

function daysInMonth(month, year) {
	return 32 - new Date(year, month, 32).getDate();
	// e.g. add 32 days to 1 April will give you 2 May. 32 - 2 gives the correct # of days in April
}


